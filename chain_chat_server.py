import socket
import select
import DBHandler


PORT = 1025
HOME = '0.0.0.0'
SIZE_OF_BUFFER = 2048000
DB_FILE_NAME = "TFD_DB.db"
server_commands = {'command_from_server': 'server : ', 'new_user': 'New user joined the group.',
                   'miner_approved': 'You are the miner!',
                   'miner_disapproved': 'You cannot be a miner, there is one already.',
                   'connection_closed_with_user': 'Connection with client {} closed.',
                   'user_left_group': '{} left the group.'}
protocol_manual_extended = {'disconnect': '/DCN', 'print': '/PNT', 'help': '/HLP', 'client_register_request': '/REG',
                            'client_register_denied': '/REG No', 'client_register_approved': '/REG Yes',
                            'client_sign_in_request': '/SIN', 'client_sign_in_denied': '/SIN No',
                            'client_sign_in_approved': '/SIN Yes', 'check_if_client_is_miner': '/IM',
                            'client_is_miner_statement': '/IM Yes'}


class ChainChatServer:
    def __init__(self):
        '''This function constructs the server and it's attributes.'''
        self.server_socket = socket.socket()
        self.server_socket.bind((HOME, PORT))
        self.server_socket.listen(5)
        self.open_client_sockets = []
        self.normal_messages_to_send = []
        self.miners_messages_to_send = []
        self.server_messages_to_send = []
        self.data_base = DBHandler.DBHandler(DB_FILE_NAME)
        self.data_base.delete_db()
        self.data_base = DBHandler.DBHandler(DB_FILE_NAME)
        self.miner_socket = None

    def send_waiting_messages(self, wlist):
        '''This function handles all the waiting messages from all kinds of user and the server itself.'''
        self.handle_miner_messages(wlist)
        self.handle_normal_messages(wlist)
        self.handle_server_messages(wlist)

    def handle_miner_messages(self, wlist):
        '''This function handles the miner's messages.'''
        for message in self.miners_messages_to_send:
            (client_socket, data1) = message
            for socket1 in wlist:
                if socket1 != client_socket:
                    socket1.send(data1)
            self.miners_messages_to_send.remove(message)

    def handle_normal_messages(self, wlist):
        '''This function handles the normal client's messages.'''
        for message in self.normal_messages_to_send:
            (client_socket, data1) = message
            for socket1 in wlist:
                if socket1 == self.miner_socket:
                    socket1.send(data1)
            self.normal_messages_to_send.remove(message)

    def handle_server_messages(self, wlist):
        '''This function handles the server's messages.'''
        for message in self.server_messages_to_send:
            (client_socket, data1) = message
            for socket1 in wlist:
                if client_socket == socket1 or client_socket == '':
                    socket1.send(server_commands['command_from_server'] + data1)
            self.server_messages_to_send.remove(message)

    def connect_new_user(self):
        '''This function connects a new client to the server's socket.'''
        (new_socket, address) = self.server_socket.accept()
        self.open_client_sockets.append(new_socket)
        print server_commands['new_user']
        if self.miner_socket is not None:
            self.server_messages_to_send.append(('', server_commands['new_user']))

    def handle_client_registration(self, current_socket, data):
        '''This function handles the registration of a new client to the DB.'''
        username = data[5:data.find(' ', 6)]
        password = data[data.find(' ', 6) + 1:]
        list_of_same_username_users = self.data_base.get_users(username)
        if len(list_of_same_username_users) > 0:
            self.server_messages_to_send.append((current_socket,
                                                 protocol_manual_extended['client_register_denied']))
        else:
            self.server_messages_to_send.append((current_socket,
                                                 protocol_manual_extended['client_register_approved']))
            self.data_base.add_user(username, password)

    def handle_client_sign_in(self, current_socket, data):
        '''This function handles client's request to sign-in to the DB.'''
        username = data[5:data.find(' ', 6)]
        password = data[data.find(' ', 6) + 1:]
        list_of_same_users = self.data_base.get_users(username, password)
        if len(list_of_same_users) == 1:
            self.server_messages_to_send.append((current_socket,
                                                 protocol_manual_extended['client_sign_in_approved']))
        else:
            self.server_messages_to_send.append((current_socket,
                                                 protocol_manual_extended['client_sign_in_denied']))

    def handle_client_check_is_miner_statement(self, current_socket, data):
        '''This function checks if a client wants to be a miner and if he can be one.'''
        if data[:7] == protocol_manual_extended['client_is_miner_statement']:
            if self.miner_socket is None:
                self.miner_socket = current_socket
                self.server_messages_to_send.append((current_socket, server_commands['miner_approved']))
            else:
                self.server_messages_to_send.append((current_socket,
                                                     server_commands['miner_disapproved']))

    def handle_client_disconnection(self, current_socket, data):
        '''This function handles client disconnection request.'''
        if current_socket == self.miner_socket:
            self.miner_socket = None
        self.open_client_sockets.remove(current_socket)
        print server_commands['connection_closed_with_user'].format(data[:data.find(' ')])
        self.server_messages_to_send.append(('', server_commands['user_left_group']
                                             .format(data[:data.find(' ')])))

    def handle_messages(self, current_socket, data):
        '''This function appends a message to sending list according to whether or not he is the miner.'''
        if current_socket == self.miner_socket:
            self.miners_messages_to_send.append((current_socket, data))
        else:
            self.normal_messages_to_send.append((current_socket, data))

    def handle_data(self, current_socket, data):
        '''This function handles data sent from the current user.'''
        print data
        if data[:4] == protocol_manual_extended['client_register_request']:
            self.handle_client_registration(current_socket, data)
        elif data[:4] == protocol_manual_extended['client_sign_in_request']:
            self.handle_client_sign_in(current_socket, data)
        elif data[:3] == protocol_manual_extended['check_if_client_is_miner']:
            self.handle_client_check_is_miner_statement(current_socket, data)
        elif data[data.find(' : ') + 3:] == protocol_manual_extended['disconnect']:
            self.handle_client_disconnection(current_socket, data)
        else:
            self.handle_messages(current_socket, data)

    def server_cycle(self):
        '''This function is the full miner cycle.
        It handles all the messages that the clients send and sends them according to the chain-chat protocol.'''
        while True:
            rlist, wlist, xlist = select.select([self.server_socket] + self.open_client_sockets,
                                                self.open_client_sockets, [])
            for current_socket in rlist:
                if current_socket is self.server_socket:
                    self.connect_new_user()
                else:
                    data = str(current_socket.recv(SIZE_OF_BUFFER))
                    self.handle_data(current_socket, data)
            self.send_waiting_messages(wlist)


def main():
    chain_chat_server = ChainChatServer()
    chain_chat_server.server_cycle()
    for client_socket in chain_chat_server.open_client_sockets:
        client_socket.close()
    chain_chat_server.server_socket.close()


if __name__ == '__main__':
    main()
