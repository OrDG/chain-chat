import time
import hashlib
import multiprocessing

DIFFICULTY = 4
INVALID_TYPE = "'{}' is not '{}' type"
INVALID_TYPE_OR = "'{}' is not '{}' type or {}"
INVALID = "Invalid {}"
WARNING = 'Something went wrong'


class Block:
    def __init__(self, index, previous_hash, timestamp, data, nonce):
        '''This function constructs the a block which assembles the block-chain.'''
        self.index = index
        self.previous_hash = previous_hash
        self.timestamp = timestamp - timestamp % 0.01 + 0.01 if timestamp % 0.01 >= 0.005 \
            else timestamp - timestamp % 0.01
        self.data = data
        self.difficulty = DIFFICULTY
        self.nonce = nonce
        self.hash = hashlib.sha256(str(self.index) + self.previous_hash + str(self.timestamp) + self.data +
                                   str(self.difficulty) + str(self.nonce)).hexdigest()


class BlockChain:
    def __init__(self, genesis_block=Block(0, '', time.time(), '', 0)):
        '''This function constructs the block-chain while checking the validity of the genesis block.'''
        self.block_chain = []
        if is_new_block_valid_structure(genesis_block) and self.is_new_block_valid_content(genesis_block):
            self.block_chain.append(genesis_block)
            if not has_work_of_proof(genesis_block):
                self.do_work_of_proof(0)

    def generate_a_block(self, string):
        '''This function generates a block and adds it to the block-chain.'''
        new_block = Block(len(self.block_chain), self.block_chain[len(self.block_chain) - 1].hash, time.time(),
                          string, 0)
        self.add_block(new_block)
        return new_block

    def add_block(self, new_block):
        '''This function checks if the new block is valid by structure and content, then does on it work-of-proof.'''
        if is_new_block_valid_structure(new_block) and self.is_new_block_valid_content(new_block):
            self.block_chain.append(new_block)
            if not has_work_of_proof(new_block):
                self.do_work_of_proof(len(self.block_chain) - 1)

    def convert_block_chain_to_dict(self):
        '''This function converts the block-chain object to dict.'''
        block_chain_dict = {}
        counter = 0
        for block in self.block_chain:
            if counter == block.index:
                block_chain_dict[str(block.index)] =\
                    [block.index, block.previous_hash, block.timestamp, block.data, block.nonce]
            else:
                print Warning
            counter += 1
        return block_chain_dict

    def is_new_block_valid_content(self, new_block):
        '''This function checks if the new-block is valid by its content.'''
        valid = True
        if new_block.index != len(self.block_chain):
            print INVALID.format('index')
            valid = False
        if new_block.index != 0 and new_block.previous_hash != self.block_chain[len(self.block_chain) - 1].hash:
            print INVALID.format('previous hash')
            valid = False
        correct_hash = hashlib.sha256(str(new_block.index) + new_block.previous_hash + str(new_block.timestamp) +
                                      new_block.data + str(new_block.difficulty) + str(new_block.nonce)).hexdigest()
        if new_block.hash != correct_hash:
            print INVALID.format('hash')
            valid = False
        if new_block.difficulty != DIFFICULTY:
            print INVALID.format('difficulty')
            valid = False
        return valid

    def do_work_of_proof(self, block_index):
        '''This function does proof-of-work on the block in the block-chain by given index.
        This function uses all the cores of the computer simultaneously.'''
        right_nonce = multiprocessing.Value('i', 0)
        someone_printing = multiprocessing.Value('b', False)
        cpu_num = multiprocessing.cpu_count()
        processes = []
        block = self.block_chain[block_index]

        for num in xrange(0, cpu_num):
            processes.append(multiprocessing.Process(target=mining, args=(right_nonce, cpu_num, num, block,
                                                                          someone_printing)))

        for process in processes:
            process.start()

        for process in processes:
            process.join()

        self.block_chain[block_index].nonce = right_nonce.value
        self.block_chain[block_index].hash = hashlib.sha256(str(block.index) + block.previous_hash +
                                                            str(block.timestamp) + block.data + str(block.difficulty) +
                                                            str(block.nonce)).hexdigest()


def mining(right_nonce, cpu_num, starting_index, block, someone_printing):
    '''This function does work-of-proof on a single block by a single core.
    This function is built to work on all the cores of the computer.'''
    block.nonce = starting_index
    while right_nonce.value == 0:
        if has_work_of_proof(block):
            right_nonce.value = block.nonce
            break
        block.nonce += cpu_num
        block.hash = hashlib.sha256(str(block.index) + block.previous_hash + str(block.timestamp) + block.data +
                                    str(block.difficulty) + str(block.nonce)).hexdigest()
        if not someone_printing.value:
            someone_printing.value = True
            print block.hash
            someone_printing.value = False


def has_work_of_proof(check_block):
    '''This function checks if work-of-proof was done on a block.'''
    checker = ''
    for num in xrange(0, DIFFICULTY):
        checker += '0'
    return True if check_block.hash[:DIFFICULTY] == checker else False


def is_new_block_valid_structure(new_block):
    '''This function checks if the structure of the block is valid by types.'''
    try:
        if type(new_block.index) != int:
            print INVALID_TYPE.format('Index', 'int')
            return False
        if type(new_block.previous_hash) != str and type(new_block.previous_hash) != unicode:
            print INVALID_TYPE_OR.format('Hash', 'str', 'unicode')
            return False
        if type(new_block.timestamp) != float:
            print INVALID_TYPE.format('Timestamp', 'float')
            return False
        if type(new_block.data) != str and type(new_block.data) != unicode:
            print INVALID_TYPE_OR.format('Data', 'str', 'unicode')
            return False
        if type(new_block.difficulty) != int:
            print INVALID_TYPE.format('Difficulty', 'int')
            return False
        if type(new_block.nonce) != int:
            print INVALID_TYPE.format('Nonce', 'int')
            return False
        return True
    except AttributeError:
        print INVALID_TYPE.format('New block', 'block')
        return False


def build_block_generator(block_chain_dict):
    '''This function returns the genesis block from a dictionary of a block-chain.'''
    index = block_chain_dict[str(0)][0]
    previous_hash = block_chain_dict[str(0)][1]
    timestamp = block_chain_dict[str(0)][2]
    data = block_chain_dict[str(0)][3]
    nonce = block_chain_dict[str(0)][4]
    genesis_block = Block(index, previous_hash, timestamp, data, nonce)
    return genesis_block


def convert_dict_to_block_chain(block_chain_dict):
    '''This function converts a dictionary of a block-chain into a block-chain object and returns it.'''
    genesis_block = build_block_generator(block_chain_dict)
    block_chain = BlockChain(genesis_block)
    for index in xrange(1, len(block_chain_dict)):
        index = block_chain_dict[str(index)][0]
        previous_hash = block_chain_dict[str(index)][1]
        timestamp = block_chain_dict[str(index)][2]
        data = str(block_chain_dict[str(index)][3])
        nonce = block_chain_dict[str(index)][4]

        tmp_block = Block(index, previous_hash, timestamp, data, nonce)

        block_chain.add_block(tmp_block)
    return block_chain
