import sqlite3
import os


DB_FILE_NAME = "CHECK_DB.db"
SELECT_CREDENTIALS = "SELECT * from USERS where username='{}' AND password='{}'"
SELECT_CREDENTIALS_USERNAME = "SELECT * from USERS where username='{}'"
SELECT_CREDENTIALS_ALL = "SELECT * from USERS"
INSERT_NEW_USER = "INSERT INTO USERS VALUES ('{}', '{}')"
CREATE_TABLE = '''CREATE TABLE USERS
                     (username text, password text)'''


class DBHandler:
    def __init__(self, db_file_name=DB_FILE_NAME):
        '''This function constructs the DB and it's attributes.'''
        self.db_file_name = db_file_name
        self.conn = sqlite3.connect(db_file_name)
        self.cursor = self.conn.cursor()
        try:
            self.cursor.execute(CREATE_TABLE)
        except sqlite3.OperationalError as ex:
            print ex.message

    def add_user(self, username, password):
        '''This function adds a user to the DB by his username and password.'''
        self.cursor.execute(INSERT_NEW_USER.format(username, password))
        self.conn.commit()

    def get_users(self, username='', password=''):
        '''This function returns a list of users, based on what attributes the user is searching for.'''
        list_of_users = []
        if username == '' and password == '':
            cursor = self.conn.execute(SELECT_CREDENTIALS_ALL)
        elif password == '':
            cursor = self.conn.execute(SELECT_CREDENTIALS_USERNAME.format(username))
        else:
            cursor = self.conn.execute(SELECT_CREDENTIALS.format(username, password))
        for row in cursor:
            list_of_users.append(row)
        return list_of_users

    def delete_db(self):
        '''This function closes the connection between the user and the DB and deletes the DB.'''
        self.conn.close()
        os.remove(self.db_file_name)

    def close_conn(self):
        '''This function closes the connection between the user and the DB.'''
        self.conn.close()
