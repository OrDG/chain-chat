import socket
import select
import msvcrt
import sys
import block
import json
import hashlib

PORT = 1025
HOME = '127.0.0.1'
SIZE_OF_BUFFER = 2048000
SIZE_OF_SMALL_BUFFER = 1024
SENT_FROM_CLIENT = '@@@'
SENT_FROM_MINER = '###'
help_manual = {'string': 'sends this string to all users', '/DCN': 'disconnect from the server.',
               '/PNT': 'print the current block chain.', '/HLP': 'opens this menu.'}
protocol_manual = {'disconnect': '/DCN', 'print': '/PNT', 'help': '/HLP'}
entering_logo = '***********************************************************************' \
          '\n********************Welcome to Chain-Chat******************************\n' \
          '***********************************************************************\n'
interface_commands = {'enter_password': 'Enter your password: ', 'enter_password_again': 'Enter your password again: ',
                      'oops_check_password': 'Oops, your check password is wrong.', 'register_or_sign_in':
                          'Do you want to register or to sign-in? ', 'register_or_sign_in_again':
                          'Please enter your way of entering the chat again: ',
                      'enter_username': 'Enter your username: ',
                      'oops_username_space': 'Oops, you cannot enter a username with space, please enter again:',
                      'oops_username_taken': 'The username is already taken, please register again.',
                      'oops_username_password': 'username or password is wrong.', 'is_miner':
                          'Are you the miner? Please answer Yes or No : ', 'again_is_miner':
                          'Please enter again Yes or No : ', 'there_is_miner':
                          'server : You cannot be a miner, there is one already.', 'command_from_server': 'server : '}


class ChainChatClient:
    def __init__(self):
        '''This function constructs the client of chain-chat and it's attributes.'''
        self.my_socket = socket.socket()
        self.my_socket.connect((HOME, PORT))
        self.name = ''
        self.is_miner = False

    def get_username(self):
        '''This function get the client's username.'''
        name = raw_input(interface_commands['enter_username'])
        while name.find(' ') != -1:
            name = raw_input(interface_commands['oops_username_space'])
        self.name = name

    def connect_to_db(self):
        '''This function connects the client to the server's DB.'''
        way_of_entering = get_way_of_entering()
        self.get_username()
        if way_of_entering == 'register':
            self.register_to_server()
        else:
            self.sign_in_to_server()

    def register_to_server(self):
        '''This function handles the client's request to register to the server's DB'''
        password = get_right_password()
        self.my_socket.send('/REG ' + self.name + ' ' + str(hashlib.sha256(password).hexdigest()))
        answer = self.my_socket.recv(SIZE_OF_SMALL_BUFFER)
        while answer == 'server : /REG No':
            print interface_commands['oops_username_taken']
            self.get_username()
            password = get_right_password()
            self.my_socket.send('/REG ' + self.name + ' ' + str(hashlib.sha256(password).hexdigest()))
            answer = self.my_socket.recv(SIZE_OF_SMALL_BUFFER)

    def sign_in_to_server(self):
        '''This function handles the client's request to sign-in and is checked on the server's DB.'''
        password = get_pass(interface_commands['enter_password'])
        self.my_socket.send('/SIN ' + self.name + ' ' + str(hashlib.sha256(password).hexdigest()))
        answer = self.my_socket.recv(SIZE_OF_SMALL_BUFFER)
        while answer != 'server : /SIN Yes':
            print interface_commands['oops_username_password']
            self.get_username()
            password = get_pass(interface_commands['enter_password'])
            self.my_socket.send('/SIN ' + self.name + ' ' + str(hashlib.sha256(password).hexdigest()))
            answer = self.my_socket.recv(SIZE_OF_SMALL_BUFFER)

    def start_client(self):
        '''This function checks if the user is a normal client or a miner, then runs the right function accordingly.'''
        self.check_if_miner()
        if not self.is_miner:
            self.normal_client()
        else:
            data = self.my_socket.recv(SIZE_OF_SMALL_BUFFER)
            print data
            if data == interface_commands['there_is_miner']:
                self.normal_client()
            else:
                self.miner_client()

    def check_if_miner(self):
        '''This function checks if the client is miner and returns True or False accordingly.'''
        is_miner = raw_input(interface_commands['is_miner'])
        while is_miner != 'Yes' and is_miner != 'No':
            is_miner = raw_input(interface_commands['again_is_miner'])
        self.my_socket.send('/IM ' + is_miner)
        self.is_miner = True if is_miner == 'Yes' else False

    def normal_client(self):
        '''This function is the full normal client cycle.
        It handles the input and the output of the user, and updates the block-chain accordingly.'''
        str_block_chain, dict_block_chain, block_chain = get_first_block_chain(self.my_socket)
        inout = [self.my_socket]
        command = ''
        print_once = False
        while True:
            rlist, wlist, xlist = select.select(inout, inout, [])
            if len(wlist) != 0:
                break_loop, print_once, command, dict_block_chain = handle_input(print_once, command, self.name,
                                                                                 self.my_socket, dict_block_chain,
                                                                                 False, block_chain)
                if break_loop:
                    break
            if len(rlist) != 0:
                data = self.my_socket.recv(SIZE_OF_BUFFER)
                str_block_chain, dict_block_chain, block_chain = handle_data(data, self.name, str_block_chain,
                                                                             dict_block_chain, block_chain)

    def miner_client(self):
        '''This function is the full miner cycle.
        It handles the input and the output of the miner, and updates the block-chain accordingly.'''
        str_block_chain, dict_block_chain, block_chain = send_first_block_chain(self.my_socket)

        inout = [self.my_socket]
        command = ''
        print_once = False
        while True:
            rlist, wlist, xlist = select.select(inout, inout, [])
            if len(wlist) != 0:
                break_loop, print_once, command, dict_block_chain = handle_input(print_once, command, self.name,
                                                                                 self.my_socket, dict_block_chain,
                                                                                 True, block_chain)
                if break_loop:
                    break
            if len(rlist) != 0:
                data = self.my_socket.recv(SIZE_OF_BUFFER)
                if data[:9] != interface_commands['command_from_server']:
                    send_normal_message(True, block_chain, self.name, self.my_socket, data, SENT_FROM_CLIENT)
                print data


def get_first_block_chain(my_socket):
    '''This function waits for the first block in the block-chain to be sent, so the client could be active.
    Then, it saves the block-chain as the start, and returns the block-chain in all its forms.'''
    str_block_chain = get_str_block_chain(my_socket)
    if str_block_chain[:3] == SENT_FROM_MINER or str_block_chain[:3] == SENT_FROM_CLIENT:
        dict_block_chain = json.loads(str_block_chain[3:])
        block_chain = block.convert_dict_to_block_chain(dict_block_chain)
        print '\n' + block_chain.block_chain[len(block_chain.block_chain) - 1].data
    else:
        dict_block_chain = json.loads(str_block_chain)
        block_chain = block.convert_dict_to_block_chain(dict_block_chain)
        print str(block_chain) + '\n'
    return str_block_chain, dict_block_chain, block_chain


def handle_data(data, name, str_block_chain, dict_block_chain, block_chain):
    '''This function handles the data the client gets from the server.'''
    if data[:9] == interface_commands['command_from_server']:
        print data
    else:
        if data[:3] == '###' or data[:3] == '@@@':
            str_block_chain = data[3:]
        else:
            str_block_chain = data
        dict_block_chain = json.loads(str_block_chain)
        block_chain = block.convert_dict_to_block_chain(dict_block_chain)
        message = block_chain.block_chain[len(block_chain.block_chain) - 1].data
        if message[:len(name) + 1] != name + ' ':
            print message
    return str_block_chain, dict_block_chain, block_chain


def get_str_block_chain(my_socket):
    '''This function waits for the first block-chain to be sent to it, avoiding server messages.'''
    string = my_socket.recv(SIZE_OF_BUFFER)
    while string[0] != '{' and string[:3] != SENT_FROM_MINER and string[:3] != SENT_FROM_CLIENT:
        string = my_socket.recv(SIZE_OF_BUFFER)
    return string


def print_name(print_once, name):
    '''This function prints the name of the client once every line.'''
    if not print_once:
        sys.stdout.write(name + ' : ')
        print_once = True
    return print_once


def add_to_command(command, char):
    '''This function adds a given char to the given command and prints it.'''
    command += char
    if char == chr(8):  # backspace
        command = command[:len(command) - 2]
        sys.stdout.write(chr(0) + chr(8))
    return command


def send_normal_message(is_miner, block_chain, name, my_socket, command, sent_from=SENT_FROM_MINER):
    '''This function handles message sending in the chat from client.'''
    if is_miner:
        if sent_from == SENT_FROM_MINER:
            block_chain.generate_a_block(name + ' : ' + command)
        else:
            block_chain.generate_a_block(command)
        dict_block_chain = block_chain.convert_block_chain_to_dict()
        str_block_chain = json.dumps(dict_block_chain)
        my_socket.send(sent_from + str_block_chain)
    else:
        my_socket.send(name + ' : ' + command)


def handle_input(print_once, command, name, my_socket, dict_block_chain, is_miner, block_chain):
    '''This function handles all the input from the user.'''
    if msvcrt.kbhit():
        print_once = print_name(print_once, name)
        char = ''
        try:
            char = msvcrt.getch()
        except KeyboardInterrupt as ex:
            print ex.message
        if char != chr(8) or (char == chr(8) and len(command) > 0):  # chr(8) => backspace
            sys.stdout.write(char)
        if char != '\r':
            command = add_to_command(command, char)
        else:
            sys.stdout.write('\n')
            if command == protocol_manual['print']:
                print dict_block_chain
            elif command == protocol_manual['help']:
                print_help()
            elif command == protocol_manual['disconnect']:
                my_socket.send(name + ' : ' + command)
                return True, print_once, command, dict_block_chain
            else:
                send_normal_message(is_miner, block_chain, name, my_socket, command)
            print_once = False
            command = ''
    return False, print_once, command, dict_block_chain


def send_first_block_chain(my_socket):
    '''This function creates a new block-chain, converts it to str, sends it and returns it.'''
    block_chain = block.BlockChain()
    dict_block_chain = block_chain.convert_block_chain_to_dict()
    str_block_chain = json.dumps(dict_block_chain)
    my_socket.send(str_block_chain)
    return str_block_chain, dict_block_chain, block_chain


def get_pass(prompt):
    '''This function hides the password the user is entering.'''
    count = 0
    hide_char = '*'
    some_password = ''
    for char in prompt:
        sys.stdout.write(char)
    while True:
        char = ''
        try:
            char = msvcrt.getch()
        except KeyboardInterrupt as ex:
            print ex.message
        if char == '\r' or char == '\n':
            break
        if char == '\b':
            count -= 1
            some_password = some_password[:-1]
            if count >= 0:
                sys.stdout.write('\b \b')
        elif char == '':
            pass
        else:
            if count < 0:
                count = 0
            count += 1
            some_password += char
            sys.stdout.write(hide_char)
    sys.stdout.write('\r\n')
    return "'%s'" % some_password if some_password != '' else "''"


def get_right_password():
    '''This function checks if the password the user enters is the same password he enters in the check-password.'''
    password = get_pass(interface_commands['enter_password'])
    check_password = get_pass(interface_commands['enter_password_again'])
    while password != check_password:
        print interface_commands['oops_check_password']
        password = get_pass(interface_commands['enter_password'])
        check_password = get_pass(interface_commands['enter_password_again'])
    return password


def print_help():
    '''This function prints the help-manual to the client.'''
    for key in help_manual:
        print key + (9 - len(key))*' ' + help_manual[key]


def get_way_of_entering():
    '''This function get the way in which the user is entering the chat.'''
    way_of_entering = raw_input(interface_commands['register_or_sign_in'])
    while way_of_entering != 'register' and way_of_entering != 'sign-in':
        way_of_entering = raw_input(interface_commands['register_or_sign_in_again'])
    return way_of_entering


def main():
    chain_chat_client = ChainChatClient()

    print entering_logo
    chain_chat_client.connect_to_db()

    chain_chat_client.start_client()
    chain_chat_client.my_socket.close()


if __name__ == '__main__':
    main()
